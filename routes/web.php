<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/{url?}', function () {
//    return view('app');
//})->where('', 'list');

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth'], function () {
    Route::get('install', 'MainController@install');
    Route::get('load', 'MainController@load');

    Route::get('uninstall', function () {
        echo 'uninstall';
        return app()->version();
    });

    Route::get('remove-user', function () {
        echo 'remove-user';
        return app()->version();
    });
});
Route::any('/bc-api/{endpoint}', 'MainController@proxyBigCommerceAPIRequest')
    ->where('endpoint', 'v2\/.*|v3\/.*');

Route::get('error', 'MainController@error');

//backup
Route::get('/orderfile', 'OrderController@Backup');
Route::get('/blogfile', 'BlogController@Backup');
Route::get('/categoriesfile', 'CategoriesController@Backup');
Route::get('/userfile', 'UserController@Backup');


//Test
Route::get('/productfile', 'ProductController@Backup');
Route::get('/productbulkpricingrulesfile', 'ProdcuctBulkPricingRulesController@Backup');
Route::get('/productcomplexrulesfile', 'ProductComplexRulesController@Backup');
Route::get('/productcustomfieldsfile', 'ProductCustomFieldsController@Backup');
Route::get('/productimagesfile', 'ProductImagesController@Backup');
Route::get('/productmetafieldsfile', 'ProductMetaFieldsController@Backup');
Route::get('/productmodifilesfile', 'ProductModifilersController@Backup');
Route::get('/productoptionfile', 'ProductOptionController@backup');
Route::get('/productreviewsfile', 'ProductReviewsController@Backup');
Route::get('/prodcuctvariantsfile', 'ProductVariantsController@Backup');
Route::get('/productvideosfile', 'ProdcutVideosController@Backup');
Route::get('/GetProductOption', 'ProductOptionsController@GetProductOption');
Route::get('/allthings', 'AllController@BackupAll');


//restore file
Route::get("/restoreuser", "RestoreController@Restoreuser");
Route::get("/restoreblog", "RestoreController@RestoreBlog");
Route::get("/restoreorder", "RestoreController@RestoreOrder");
Route::get("/restoreproduct", "RestoreController@RestoreProduct");
Route::get("/restorecategories", "RestoreController@RestoreCategories");
