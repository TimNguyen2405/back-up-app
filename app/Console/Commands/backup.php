<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws FileNotFoundException
     */
    public function handle()
    {
//        get files to backup
//        $files = Storage::disk('public/Backup/')->files('product');
//        $files = Storage::disk('public/Backup/')->files('order');
//        $files = Storage::disk('public/Backup/')->files('categories');
//        $files = Storage::disk('public/Backup/')->files('user');
        $files = Storage::disk('public/Backup/')->files('blog');

        $i = 1;
        foreach ($files as $file) {
            $filename[$i]['file'] = $file;
            $i++;
        }

        $headers = [''];
        $this->table($headers, $filename);
        //ask console user for input
        $backupFilename = $this->ask('Which file would you like to restore?');
        $getBackupFile = Storage::disk('public/Backup/')->get($backupFilename);
        $backupFilename = explode("/", $backupFilename);
        Storage::disk('public/Backup')->put($backupFilename[1], $getBackupFile);
        //get file mime
        $mime = Storage::mimeType($backupFilename[1]);

        if ($mime == "application/x-gzip") {
            //mysql command to restore backup from the selected gzip file
            $command = "zcat " . storage_path() . "/" . $backupFilename[1] . " | mysql --user=" . env('BackupApp') . " --password=" . env('') . " --host=" . env('localhost') . " " . env('BackupApp') . "";
        } elseif ($mime == "text/plain") {
            //mysql command to restore backup from the selected sql file
            $command = "mysql --user=" . env('BackupApp') . " --password=" . env('') . " --host=" . env('localhost') . " " . env('BackupApp') . " < " . storage_path() . "/" . $backupFilename[1] . "";
        } else {
            //throw error if file type is not supported
            $this->error("File is not gzip or plain text");
            Storage::disk('public/Backup/')->delete($backupFilename[1]);
            return false;
        }

        if ($this->confirm("Are you sure you want to restore the database? [y|N]")) {
            $returnfile = null;
            $output = null;
            exec($command, $output, $returnfile);

            Storage::disk('public/Backup/')->delete($backupFilename[1]);

            if (!$returnfile) {
                $this->info('Database Restored');
            } else {
                $returnfile = null();
                $this->error($returnfile);
            }
        }
    }
}
