<?php

namespace App\Jobs;

use App\Console\Commands\Backup;
use App\Http\Controllers\UserController;
use http\Env\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\User;


class BackupJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $user;
    /**
     * Create a new job instance.
     *
     * @param  User  $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param  BackupJob  $processor
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Process backup...
    }

    public function store(Request $request)
    {
        $user = new UserController();
        Backup::handle($user)
            ->delay(now()->addMinutes(1));
    }
}
