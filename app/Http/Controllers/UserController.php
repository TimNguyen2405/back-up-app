<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function GetAllUser(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v2/customers.json', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json"
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($data);
    }

    public function GetUser(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v2/customers.json', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json"
            ]
        ])->getBody()->getContents();
        return($result);
    }
    public function Backup(Request $request)
    {
        $result= $this->GetUser($request);
        $name = "user"."4roq59qwri".date("dmY_hisa");
        $userfile = fopen("..\storage\app\public\Backup\User\\".$name, "a+");
        fwrite($userfile, $result);
        DB::table("backup")->insert([
            "type"=> "user",
            "store_hash"=>"4roq59qwri",
            "path"=>"\Backup\User\\".$name
        ]);
    }
}
