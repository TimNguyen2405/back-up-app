<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductImagesController extends Controller
{
    public function GetProductImages(Request $request, $id)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/products/$id/images', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json",
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($result);
    }
    public function Backup(Request $request)
    {
        $result= $this->GetProductImages($request);
        $name = "ProductImages"."4roq59qwri".date("dmY_hisa");
        $ProductImagesfile = fopen("Backup/".$name, "a+");
        fwrite($ProductImagesfile, $result);
        DB::table("backup")->insert([
            "type"=> "ProductImages",
            "store_hash"=>"4roq59qwri",
            "path"=>"Backup/".$name
        ]);
    }
}
