<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function GetCategories(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/categories', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type" => "application/json"
            ]
        ])->getBody()->getContents();
            return($result);
    }
    public function GetCat(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/categories', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json"
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return array($data);
    }
    public function Backup(Request $request)
    {
        $result= $this->GetCategories($request);
        $name = "categories_"."4roq59qwri".date("dmY_hisa");
        $categoriesfile = fopen("..\storage\app\public\Backup\Categories\\".$name, "a+");
        fwrite($categoriesfile, $result);
        DB::table("backup")->insert([
            "type"=> "categories",
            "store_hash"=>"4roq59qwri",
            "path"=>"\Backup\Categories\\".$name,
        ]);
    }
}
