<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductBulkPricingRulesController extends Controller
{
    public function GetBulkPricingRules(Request $request, $id)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/products/$id/bulk-pricing-rules', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json",
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($result);
    }
    public function Backup(Request $request)
    {
        $result = $this->GetBulkPricingRules($request);
        $name = "ProductBulkPricingRules_" . "4roq59qwri" . date("dmY_hisa");
        $ProductBulkPricingRulesfile = fopen("Backup/" . $name, "a+");
        fwrite($ProductBulkPricingRulesfile, $result);
        DB::table("backup")->insert([
            "type" => "ProductBulkPricingRules",
            "store_hash" => "4roq59qwri",
            "path" => "Backup/" . $name
        ]);
    }
}
