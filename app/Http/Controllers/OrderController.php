<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends MainController
{
    public function GetOrderInfo(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v2/orders.json', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json"
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($data);
    }

    public function GetOrder(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v2/orders.json', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json"
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
//        return array($data, $result);
        return ($result);
    }
    public function Backup(Request $request)
    {
        $result= $this->GetOrder($request);
        $name = "order_"."4roq59qwri".date("dmY_hisa");
        $orderfile = fopen("..\storage\app\public\Backup\Order\\".$name, "a+");
        fwrite($orderfile, $result);
        DB::table("backup")->insert([
            "type"=> "order",
            "store_hash"=>"4roq59qwri",
            "path"=>"\Backup\Order\\".$name
        ]);
    }
}
