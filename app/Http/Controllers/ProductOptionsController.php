<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductOptionsController extends Controller
{
    public function GetProductOptions(Request $request, $id)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/products/$id/custom-fields', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json",
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($result);
    }
    public function GetProductOption(Request $request, $id)
    {
        $client = new Client();
        $url = "api.bigcommerce.com/" . "4roq59qwri" . "/v2/products/" . $id . "/images.json";
        $result = $client->request("get", $url, [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("auth_Token"),
                "Content-Type" => "application/json",
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($data);
    }
    public function Backup(Request $request, $id)
    {
        $result= $this->GetProductOptions($request, $id);
        $name = "ProductOptions"."4roq59qwri".date("dmY_hisa");
        $ProductOptionsfile = fopen("Backup/".$name, "a+");
        fwrite($ProductOptionsfile, $result);
        DB::table("backup")->insert([
            "type"=> "ProductOptions",
            "store_hash"=>"4roq59qwri",
            "path"=>"Backup/".$name
        ]);
    }
}
