<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    public function GetAllProduct(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/products', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json"
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true)["data"];
        return ($data);
    }
    public function GetProductData(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/products', [
            "headers" => [
                "X-Auth-Client" =>  $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json"
            ]
        ])->getBody()->getContents();
        return($result);
    }
    public function Backup(Request $request)
    {
        $result= $this->GetProductData($request);
        $name = "product_"."4roq59qwri".date("dmY_hisa");
        $productfile = fopen("..\storage\app\public\Backup\Product\\".$name, "a+");
        fwrite($productfile, $result);
        DB::table("backup")->insert([
            "type"=> "product",
            "store_hash"=>"4roq59qwri",
            "path"=>"\Backup\Product\\".$name
        ]);
    }
}
