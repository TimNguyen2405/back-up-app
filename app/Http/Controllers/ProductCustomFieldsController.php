<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductCustomFieldsController extends Controller
{
    public function GetCustomFields(Request $request, $id)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/products/$id/custom-fields', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json",
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($result);
    }
    public function Backup(Request $request, $id)
    {
        $result= $this->GetCustomFields($request, $id);
        $name = "ProductComplexRules_"."4roq59qwri".date("dmY_hisa");
        $ProductCustomFieldsfile = fopen("Backup/".$name, "a+");
        fwrite($ProductCustomFieldsfile, $result);
        DB::table("backup")->insert([
            "type"=> "ProductCustomFields",
            "store_hash"=>"4roq59qwri",
            "path"=>"Backup/".$name
        ]);
    }
}
