<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class LogoController extends Controller
{
    public function GetStoreInfo(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v2/store.json', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json"
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        $store["logo"] = $data["logo"]["url"];
        $store["name"] = $data["name"];
        return ($store);
    }
}
