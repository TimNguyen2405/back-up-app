<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class MainController extends BaseController
{
    protected $baseURL;

    public function __construct()
    {
        $this->baseURL = env('APP_URL');
    }

    public function getAppClientId()
    {
        if (env('APP_ENV') === 'local') {
            return env('BC_LOCAL_CLIENT_ID');
        } else {
            return env('BC_APP_CLIENT_ID');
        }
    }

    public function getAppSecret(Request $request)
    {
        if (env('APP_ENV') === 'local') {
            return env('BC_LOCAL_SECRET');
        } else {
            return env('BC_APP_SECRET');
        }
    }

    public function getAccessToken(Request $request)
    {
        if (env('APP_ENV') === 'local') {
            return env('BC_LOCAL_ACCESS_TOKEN');
        } else {
            $context =$this->getContext($request);
            $access_token = DB::table("table_info")->where("context", "=", $context)->get("access_token")[0]->access_token;
            return $access_token;
//            return $request->session()->get('access_token');
        }
    }

    public function getStoreHash(Request $request)
    {
        if (env('APP_ENV') === 'local') {
            return env('BC_LOCAL_STORE_HASH');
        } else {
            return $request->session()->get('store_hash');
        }
    }

    public function install(Request $request)
    {
        // Make sure all required query params have been passed
        if (!$request->has('code') || !$request->has('scope') || !$request->has('context')) {
            return redirect()->action('MainController@error')->with('error_message', 'Not enough information was passed to install this app.');
        }

        try {
            $client = new Client();
            $result = $client->request('POST', 'https://login.bigcommerce.com/oauth2/token', [
                'json' => [
                    'client_id' => $this->getAppClientId(),
                    'client_secret' => $this->getAppSecret($request),
                    'redirect_uri' => $this->baseURL . '/auth/install',
                    'grant_type' => 'authorization_code',
                    'code' => $request->input('code'),
                    'scope' => $request->input('scope'),
                    "context" => $request->input("context"),
                    "access_token" => $request->access_token,
                    "store_hash" => $request->session()

                ],
            ]);
            $data = json_decode($result->getBody());
            DB::table("table_info")->insert([
                "context" => $data->context,
                "access_token" => $data->access_token,
                "scope" => $data->scope,
                "user_id" => $data->user->id,
                "username" => $data->user->username,
                "email" => $data->user->email,
            ]);

            $statusCode = $result->getStatusCode();
            $data = json_decode($result->getBody(), true);

            if ($statusCode == 200) {
                $request->session()->put('store_hash', $data['context']);
                $request->session()->put('access_token', $data['access_token']);
                $request->session()->put('user_id', $data['user']['id']);
                $request->session()->put('user_email', $data['user']['email']);

                // If the merchant installed the app via an external link, redirect back to the
                // BC installation success page for this app}
            }

            return view('welcome');
        } catch (RequestException $e) {
            $statusCode = $e->getResponse()->getStatusCode();
            $errorMessage = "An error occurred.";

            if ($e->hasResponse()) {
                if ($statusCode != 500) {
                    $errorMessage = Psr7\str($e->getResponse());
                }
            }

            // If the merchant installed the app via an external link, redirect back to the
            // BC installation failure page for this app
            if ($request->has('external_install')) {
                return redirect('https://login.bigcommerce.com/app/' . $this->getAppClientId() . '/install/failed');
            } else {
                return redirect()->action('MainController@error')->with('error_message', $errorMessage);
            }
        }
    }

    public function load(Request $request)
    {
        $signedPayload = $request->input('signed_payload');
        if (!empty($signedPayload)) {
            $verifiedSignedRequestData = $this->verifySignedRequest($signedPayload, $request);

            if ($verifiedSignedRequestData !== null) {
                $access_token = DB::table("table_info")
                    ->where("context", "=", $verifiedSignedRequestData['context'])
                    ->first()->access_token;
                $auth_client= ($this->getAppClientId());
                $request->session()->put('user_id', $verifiedSignedRequestData['user']['id']);
                $request->session()->put('user_email', $verifiedSignedRequestData['user']['email']);
                $request->session()->put('owner_id', $verifiedSignedRequestData['owner']['id']);
                $request->session()->put('owner_email', $verifiedSignedRequestData['owner']['email']);
                $request->session()->put('store_hash', $verifiedSignedRequestData['context']);
                $request->session()->put('access_token', $access_token);
                $request->session()->put('version', $access_token);
                $request->session()->put('x-auth-client', $auth_client);
            } else {
                return redirect()->action('MainController@error')->with('error_message', 'The signed request from BigCommerce could not be validated.');
            }
        }
//display content !
        $categoriescontroller = new CategoriesController();
        $categoties = ($categoriescontroller->GetCat($request));
        $blogcontroller = new BlogController();
        $blog = ($blogcontroller->GetAllBlog($request));
        $logocontroller = new LogoController();
        $logo = ($logocontroller->GetStoreInfo($request))["logo"];
        $currenciescontroller = new CurrenciesController();
        $currencies = ($currenciescontroller->GetAllCurrencies($request));
        $usercontroller = new UserController();
        $user = ($usercontroller->GetAllUser($request));
        $productcontroller = new ProductController();
        $product = ($productcontroller->GetAllProduct($request));
        $ordercontroller = new OrderController();
        $orders = ($ordercontroller->GetOrderInfo($request));
        return view('welcome', [
            "allproduct" => sizeof($product),
            "numberoforders" => sizeof($orders),
            "alluser" => sizeof($user),
            "allcurrencies" => sizeof($currencies),
            "logo" => $logo,
            "blog" => sizeof($blog),
            "categories" => sizeof($categoties),
        ]);
    }
//run data
    public function getdata(Request $request)
    {
        $allcontroller = new AllController();
        $BackupAll = $allcontroller->BackupAll($request);
        $ordercontroller = new OrderController();
        $ordercontroller->GetOrder($request);
        $productcontroller = new ProductController();
        $productcontroller->GetProductData($request);
        $blogcontroller = new BlogController();
        $blogcontroller->GetBlog($request);
        $categoriescontroller = new CategoriesController();
        $categoriescontroller->GetCat($request);
        $usercontroller = new UserController();
        $usercontroller->GetUser($request);
        $productbulkpricingrulescontroller = new ProductBulkPricingRulesController();
        $productbulkpricingrulescontroller ->GetBulkPricingRules($request);
        $productcomplexrulescontroller =new ProductComplexRulesController();
        $productcomplexrulescontroller ->GetComplexRules($request);
        $productcustomfieldscontroller =new ProductCustomFieldsController();
        $productcustomfieldscontroller ->GetCustomFields($request);
        $productimagescontroller = new ProductImagesController();
        $productimagescontroller ->GetProductImages($request);
        $productmetafieldscontroller = new ProductMetaFieldsController();
        $productmetafieldscontroller -> GetProductMetaFields($request);
        $productmodifierscontroller =new ProductModifiersController();
        $productmodifierscontroller ->GetProductModifiers($request);
        $productoptioncontroller = new ProductOptionsController();
        $productoptioncontroller ->GetProductOptions($request);
        $productreviewscontroller =new ProductReviewsController();
        $productreviewscontroller ->GetProductReviews($request);
        $productvariantscontroller = new ProductVariantsController();
        $productvariantscontroller ->GetProductVariants($request);
        $productvideoscontroller = new ProductVideosController();
        $productvideoscontroller -> GetProductVideos($request);
        $allcontroller = new AllController();
        $BackupAll = $allcontroller->BackupAll($request);
    }


    public function error(Request $request)
    {
        $errorMessage = "Internal Application Error";
        if ($request->session()->has('error_message')) {
            $errorMessage = $request->session()->get('error_message');
        }
        echo "<h4>An issue has occurred:</h4> <p>" . $errorMessage . '</p> <a href="' . $this->baseURL . '">Go back to home</a>';
    }

    private function verifySignedRequest($signedRequest, $appRequest)
    {
        list($encodedData, $encodedSignature) = explode('.', $signedRequest, 2);

        // decode the data
        $signature = base64_decode($encodedSignature);
        $jsonStr = base64_decode($encodedData);
        $data = json_decode($jsonStr, true);

        // confirm the signature
        $expectedSignature = hash_hmac('sha256', $jsonStr, $this->getAppSecret($appRequest), $raw = false);
        if (!hash_equals($expectedSignature, $signature)) {
            error_log('Bad signed request from BigCommerce!');
            return null;
        }
        return $data;
    }

    public function makeBigCommerceAPIRequest(Request $request, $endpoint)
    {
        $requestConfig = [
            'headers' => [
                'X-Auth-Client' => $this->getAppClientId(),
                'X-Auth-Token' => $this->getAccessToken($request),
                'Content-Type' => 'application/json',
            ],
        ];

        if ($request->method() === 'PUT') {
            $requestConfig['body'] = $request->getContent();
        }

        $client = new Client();
        $result = $client->request($request->method(), 'https://api.bigcommerce.com/' . $this->getStoreHash($request) . '/' . $endpoint, $requestConfig);
        return $result;
    }

    public function proxyBigCommerceAPIRequest(Request $request, $endpoint)
    {
        if (strrpos($endpoint, 'v2') !== false) {
            // For v2 endpoints, add a .json to the end of each endpoint, to normalize against the v3 API standards
            $endpoint .= '.json';
        }

        $result = $this->makeBigCommerceAPIRequest($request, $endpoint);

        return response($result->getBody(), $result->getStatusCode())->header('Content-Type', 'application/json');
    }
}
