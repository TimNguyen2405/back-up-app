<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\Http\Controllers\ProductController as Products;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RestoreController extends Controller
{
    public function Restoreuser(Request $request)
    {
        $paths = DB::table('backup')->select("path")->where("type", "=", "user")->get();
        $client = new Client(['base_uri' => 'https://api.bigcommerce.com/' . $request->session()->get("store_hash") . '/v3/customers']);
        foreach ($paths as $path) {
            $mypath = $path->path;
            $users = Storage::get($mypath);
            $userData = json_decode($users);
            foreach ($userData as $useritem) {
                $client->request("post", 'https://api.bigcommerce.com/' . $request->session()->get("store_hash") . '/v3/customers', [
                    "headers" => [
                        'Content-Type' => 'application/json',
                        "X-Auth-Client" => $request->session()->get("auth_client"),
                        "X-Auth-Token" => $request->session()->get("access_token"),
                    ],
                    "json" => [
                        [
                            'company' => $useritem->company,
                            'first_name' => $useritem->first_name,
                            'last_name' => $useritem->last_name,
                            'email' => $useritem->email,
                            'form_fields' => $useritem->form_fields,
                            'date_created' => $useritem->date_created,
                            'date_modified' => $useritem->date_modified,
                            'store_credit' => $useritem->store_credit,
                            'customer_group_id' => $useritem->customer_group_id,
                            'tax_exempt_category' => $useritem->tax_exempt_category,
                            'reset_pass_on_login' => $useritem->reset_pass_on_login,
                            'accepts_marketing' => $useritem->accepts_marketing,
                        ]
                    ]
                ]);
            }
        }
    }


    public function RestoreBlog(Request $request)
    {
        $paths = DB::table('backup')->select("path")->where("type", "=", "blog")->get();
        $client = new Client();
        foreach ($paths as $path) {
            $mypath = $path->path;
            $blogs = Storage::get($mypath);
            $blogData = json_decode($blogs, true);
            foreach ($blogData as $blogitem) {
                $client->request("post", 'https://api.bigcommerce.com/' . $request->session()->get("store_hash") . '/v2/blog/posts.json', [
                    "headers" => [
                        'Content-Type' => 'application/json',
                        "X-Auth-Client" => $request->session()->get("auth_client"),
                        "X-Auth-Token" => $request->session()->get("access_token"),
                    ],
                    "json" => [
                        [
                            'title' => $blogitem['title'],
                            'body' => $blogitem['body'],
                            'tags' => $blogitem['tags'],
                            'is_published' => $blogitem['is_published'],
                            'published_date' => $blogitem['published_date'],
                            'author' => $blogitem['author'],
                            'thumbnail_path' => $blogitem['thumbnail_path'],
                        ]
                    ]
                ]);
            }
        }
    }


    public function RestoreOrder(Request $request)
    {
        $orderpaths = DB::table('backup')->select("path")->where("type", "=", "order")->get();
        $productpaths = DB::table('backup')->select("path")->where("type", "=", "product")->get();
        $client = new Client();
//        $client = new Client(['base_uri' => 'https://api.bigcommerce.com/' . $request->session()->get("store_hash") . '/v2/orders']);
        foreach ($orderpaths as $orderpath) {
            $mypath = $orderpath->path;
            $order = Storage::get($mypath);
            $product = Storage::get($productpaths);
            $orderData = json_decode($order, true);
            $productData = json_decode($product, true);
            foreach ($productData as $productitem) {
                foreach ($orderData as $orderitem) {
                    $client->request("put", 'https://api.bigcommerce.com/' . $request->session()->get("store_hash") . '/v2/orders', [
                        "headers" => [
                            'Content-Type' => 'application/json',
                            "X-Auth-Client" => $request->session()->get("auth_client"),
                            "X-Auth-Token" => $request->session()->get("access_token"),
                        ],
                        "json" => [
                            [
                            "id" => $orderitem['id'],
                            "customer_id" => $orderitem['customer_id'],
                            "date_created" => $orderitem['date_created'],
                            "status_id" => $orderitem['status_id'],
                            "subtotal_ex_tax" => $orderitem['subtotal_ex_tax'],
                            "subtotal_inc_tax" => $orderitem['subtotal_inc_tax'],
                            "base_shipping_cost" => $orderitem['base_shipping_cost'],
                            "shipping_cost_ex_tax" => $orderitem['shipping_cost_ex_tax'],
                            "shipping_cost_inc_tax" => $orderitem['shipping_cost_inc_tax'],
                            "base_handling_cost" => $orderitem['base_handling_cost'],
                            "handling_cost_ex_tax" => $orderitem['handling_cost_ex_tax'],
                            "handling_cost_inc_tax" => $orderitem['handling_cost_inc_tax'],
                            "base_wrapping_cost" => $orderitem['base_wrapping_cost'],
                            "wrapping_cost_ex_tax" => $orderitem['wrapping_cost_ex_tax'],
                            "wrapping_cost_inc_tax" => $orderitem['wrapping_cost_inc_tax'],
                            "total_ex_tax" => $orderitem['total_ex_tax'],
                            "total_inc_tax" => $orderitem['total_inc_tax'],
                            "items_total" => $orderitem['items_total'],
                            "items_shipped" => $orderitem['items_shipped'],
                            "payment_method" => $orderitem['payment_method'],
                            "payment_provider_id" => $orderitem['payment_provider_id'],
                            "refunded_amount" => $orderitem['refunded_amount'],
                            "order_is_digital" => $orderitem['order_is_digital'],
                            "ip_address" => $orderitem['ip_address'],
                            "geoip_country" => $orderitem['geoip_country'],
                            "default_currency_code" => $orderitem['default_currency_code'],
                            "staff_notes" => $orderitem['staff_notes'],
                            "customer_message" => $orderitem['customer_message'],
                            "discount_amount" => $orderitem['discount_amount'],
                            "ebay_order_id" => $orderitem['ebay_order_id'],
                            "order_source" => $orderitem['order_source'],
                            "channel_id" => $orderitem['channel_id'],
                            "external_source" => $orderitem['external_source'],
                            "products" => [
                                "url" => $productitem['url'],
                                "resource" => $productitem['resource'],
                            ],
                            "billing_address" => $orderitem['billing_address']
                            ]]
                    ]);
                }
            }
        }
    }

    public function RestoreProduct(Request $request)
    {

        $productpaths = DB::table('backup')->select("path")->where("type", "=", "product")->get();
        $client = new Client();
//        $client = new Client(['base_uri' => 'https://api.bigcommerce.com/' . $request->session()->get("store_hash") . '/v3/catalog/products']);
        foreach ($productpaths as $path) {
            $mypath = $path->path;
            $product = Storage::get($mypath);
            $productData = json_decode($product, true)["data"];

            foreach ($productData as $productitem) {
//                [
                    $result = $client->request("post", 'https://api.bigcommerce.com/' . $request->session()->get("store_hash") . '/v3/catalog/products', [
                        "headers" => [
                            'Content-Type' => 'application/json',
                            "X-Auth-Client" => $request->session()->get("auth_client"),
                            "X-Auth-Token" => $request->session()->get("access_token"),
                        ],
                        "json" => [[
                                "name" => $productitem['name'],
                                "type" => $productitem['type'],
                                "sku" => $productitem['sku'],
                                "title" => $productitem['page_title'],
                                "description" => $productitem['description'],
                                "weight" => $productitem['weight'],
                                "width" => $productitem['width'],
                                "depth" => $productitem['depth'],
                                "height" => $productitem['height'],
                                "price" => $productitem['price'],
                                "cost_price" => $productitem['cost_price'],
                                "retail_price" => $productitem['retail_price'],
                                "sale_price" => $productitem['sale_price'],
                                "map_price" => $productitem['map_price'],
                                "tax_class_id" => $productitem['tax_class_id'],
                                "product_tax_code" => $productitem['product_tax_code'],
                                "calculated_price" => $productitem['calculated_price'],
                                "categories" => $productitem['categories'],
                                "brand_id" => $productitem['brand_id'],
                                "option_set_id" => $productitem['option_set_id'],
                                "option_set_display" => $productitem['option_set_display'],
                                "inventory_level" => $productitem['inventory_level'],
                                "inventory_warning_level" => $productitem['inventory_warning_level'],
                                "inventory_tracking" => $productitem['inventory_tracking'],
                                "reviews_rating_sum" => $productitem['reviews_rating_sum'],
                                "reviews_count" => $productitem['reviews_count'],
                                "total_sold" => $productitem['total_sold'],
                                "fixed_cost_shipping_price" => $productitem['fixed_cost_shipping_price'],
                                "is_free_shipping" => $productitem['is_free_shipping'],
                                "is_visible" => $productitem['is_visible'],
                                "is_featured" => $productitem['is_featured'],
                                "related_products" => $productitem['related_products'],
                                "warranty" => $productitem['warranty'],
                                "bin_picking_number" => $productitem['bin_picking_number'],
                                "layout_file" => $productitem['layout_file'],
                                "upc" => $productitem['upc'],
                                "mpn" => $productitem['mpn'],
                                "gtin" => $productitem['gtin'],
                                "search_keywords" => $productitem['search_keywords'],
                                "availability" => $productitem['availability'],
                                "availability_description" => $productitem['availability_description'],
                                "gift_wrapping_options_type" => $productitem['gift_wrapping_options_type'],
                                "sort_order" => $productitem['sort_order'],
                                "condition" => $productitem['condition'],
                                "is_condition_shown" => $productitem['is_condition_shown'],
                                "order_quantity_minimum" => $productitem['order_quantity_minimum'],
                                "order_quantity_maximum" => $productitem['order_quantity_maximum'],
                                "page_title" => $productitem['page_title'],
                                "meta_keywords" => $productitem['meta_keywords'],
                                "meta_description" => $productitem['meta_description'],
                                "date_created" => $productitem['date_created'],
                                "date_modified" => $productitem['date_modified'],
                                "view_count" => $productitem['view_count'],
                                "preorder_release_date" => $productitem['preorder_release_date'],
                                "preorder_message" => $productitem['preorder_message'],
                                "is_preorder_only" => $productitem['is_preorder_only'],
                                "is_price_hidden" => $productitem['is_price_hidden'],
                                "price_hidden_label" => $productitem['price_hidden_label'],
                                "custom_url" => $productitem['custom_url'],
                        ]]]);
//                ];
            }
        }
    }
}
