<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AllController extends Controller
{
    public function BackupAll(Request $request)
    {
        $a = new CategoriesController();
        $a->Backup($request);
        $b = new UserController();
        $b->Backup($request);
        $c =new  OrderController();
        $c ->Backup($request);
        $d = new ProductController();
        $d ->Backup($request);
        $e = new BlogController();
        $e ->Backup($request);
    }
}
