<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductVariantsController extends Controller
{
    public function GetProductVariants(Request $request, $id)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v3/catalog/products/$id/variants', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type"=>"application/json",
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($result);
    }
    public function Backup(Request $request)
    {
        $result= $this->GetProductVariants($request);
        $name = "ProductVariants"."4roq59qwri".date("dmY_hisa");
        $ProductVariantsfile = fopen("Backup/".$name, "a+");
        fwrite($ProductVariantsfile, $result);
        DB::table("backup")->insert([
            "type"=> "ProductVariants",
            "store_hash"=>"4roq59qwri",
            "path"=>"Backup/".$name
        ]);
    }
    public function GetProductID(Request $request, $id)
    {
        $client = new Client();
        $url = "api.bigcommerce.com/" . "4roq59qwri" . "/v2/products/" . $id . "/images.json";
        $result = $client->request("get", $url, [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("auth_Token"),
                "Content-Type" => "application/json",
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($data);
    }

}
