<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function GetAllBlog(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v2/blog/posts.json', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type" => "application/json"
            ]
        ])->getBody()->getContents();
        $data = json_decode($result, true);
        return ($data);
    }

    public function GetBlog(Request $request)
    {
        $client = new Client();
        $result = $client->request("get", 'https://api.bigcommerce.com/'.$request->session()->get("store_hash").'/v2/blog/posts.json', [
            "headers" => [
                "X-Auth-Client" => $request->session()->get("auth_client"),
                "X-Auth-Token" => $request->session()->get("access_token"),
                "Content-Type" => "application/json"
            ]
        ])->getBody()->getContents();
        return($result);
    }
    public function Backup(Request $request)
    {
        $result= $this->GetBlog($request);
        $name = "blog"."4roq59qwri".date("dmY_hisa");
        $blogfile = fopen("..\storage\app\public\Backup\Blog\\".$name, "a+");
        fwrite($blogfile, $result);
        DB::table("backup")->insert([
           "type"=> "blog",
            "store_hash"=>"4roq59qwri",
            "path"=>"\Backup\Blog\\".$name
        ]);
    }
}

