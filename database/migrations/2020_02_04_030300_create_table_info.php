<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("context");
            $table->string("access_token") ;
            $table->string("scope", 1000);
            $table->string("user_id");
            $table->string("username") ;
            $table->string("email");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_info');
    }
}
